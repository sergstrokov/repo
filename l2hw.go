package main

import (
	"fmt"
	"math"
	"os"
)

func main() {
	fmt.Println("Добро пожаловать в центр рассчетов")
	var path uint
	fmt.Println("Введите номер соответствующий задаче: 1. Вычисление площади прямоугольника 2.Работа с окружностью 3.Формат числа")
	_,err := fmt.Scan(&path)
	if err != nil {
		fmt.Println("Ошибка", err.Error())
		os.Exit(1)
	}
	if path > 3 {
		fmt.Println("Ошибка, данной функции калькулятор не предусматривает")
		os.Exit(2)
	}
	var res,dia,leng float64
	var points,tens,hundred uint
	switch path {
		case 1:
			var a,b float64
			fmt.Println("Введите размер сторон прямоугольника")
			_,err := fmt.Scan(&a,&b)
			if err != nil {
				fmt.Println("Ошибка", err.Error())
				os.Exit(3)}
			res = a * b
			fmt.Println(res)
	case 2:
		var a float64
		fmt.Println("Введите площадь круга")
		_,err := fmt.Scan(&a)
		if err != nil {
			fmt.Println("Ошибка", err.Error())
			os.Exit(3)}
		dia = math.Sqrt(a*4/math.Pi)
		leng = dia*math.Pi
		fmt.Println("диаметр круга" , dia)
		fmt.Println("Длинна окружности" , leng)
	case 3:
		var a uint
		fmt.Println("Введите число")
		_,err := fmt.Scan(&a)
		if err != nil {
			fmt.Println("Ошибка", err.Error())
			os.Exit(3)}
			hundred = (a/100)
			tens= ((a - hundred*100) / 10)
		    points= (a-hundred*100-tens*10)
		fmt.Println("сотни", hundred)
		fmt.Println("десятки", tens)
		fmt.Println("единицы", points)

	}

}
